package stepDef;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DatatableTesting {

    private List<RandomData> response = new ArrayList<>();
    @When("I test the datatable")
    public void datatable(List<RandomData> randomData) {

        response = apiToBeTested(randomData);
    }

//    private List<RandomData> apiToBeTested(List<RandomData> randomData) {
//        return randomData.stream().map(randomData1 -> {
//            return new RandomData(randomData1.arbit + "9", randomData1.value);
//        }).collect(Collectors.toList());
//    }

    // here map function is from Function interface
    private List<RandomData> apiToBeTested(List<RandomData> randomData) {
        return randomData.stream().map(randomData1 -> new RandomData(randomData1.arbit + "9", randomData1.value)).collect(Collectors.toList());
    }

    @Then("I see the following")
    public void assertEquals(List<RandomData> expected) {
        Assert.assertEquals(expected, response);
    }
}
