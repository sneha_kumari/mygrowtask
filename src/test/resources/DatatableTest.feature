Feature: Working with datatable

  @RunOnly
  Scenario: Working with datatable
    When I test the datatable
      | Arbit | Value      |
      | 1     | first_val  |
      | 2     | second_val |
    Then I see the following
      | Arbit | Value      |
      | 19    | first_val  |
      | 29    | second_val |